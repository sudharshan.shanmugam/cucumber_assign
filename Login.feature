Feature: user should able to login with valid credentials

Scenario Outline: verify LoginPage
Given open the browser 
When user should enter valid <username> in email textbox
And user should enter valid <Password> in password textbox
Then user should click login button


  Examples: 
      | username                     | Password | 
      | "sudhar3298@gmail.com"     | "Sudhar@4200" | 
      | "sudhar3298@gmail.com"     | "Sudhar@4200" |     
